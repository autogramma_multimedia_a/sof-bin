#!/bin/bash

SOF_DIR=~/sof

SOF_PLATFORM=imx8m
TPLG_PLATFORM=imx8mp-wm8904

SOF_BIN=${SOF_DIR}/sof/installer/staging/

if [ ! -L ${SOF_BIN}/sof ]; then
	echo "ERROR: SOF binaries directory doesn't exist"
	exit 1
fi

if [ ! -L ${SOF_BIN}/sof-tplg ]; then
	echo "ERROR: SOF topology directory doesn't exist"
	exit 1
fi

die() {
	echo "Something went wrong!!!"
	exit 2
}

SOF_VERSION=$(readlink ${SOF_BIN}/sof)
SOF_TPLG_VERSION=$(readlink ${SOF_BIN}/sof-tplg)

echo "* Cleaning current SOF binaries"
rm -vrf sof-* sof

echo "* New SOF binaries ver:" ${SOF_VERSION}

echo "* Creating new directories..."
mkdir -v -p ${SOF_VERSION} || die
mkdir -v -p ${SOF_TPLG_VERSION} || die

echo "* Copying new binaries..."
cp -v ${SOF_BIN}/${SOF_VERSION}/sof-${SOF_PLATFORM}.* ${SOF_VERSION} || die

echo "* Copying new topology files..."
cp -v ${SOF_BIN}/${SOF_TPLG_VERSION}/sof-${TPLG_PLATFORM}* ${SOF_TPLG_VERSION} || die

echo "* Installing new symlink"
ln -vsf ${SOF_VERSION} sof || die
ln -vsf ${SOF_TPLG_VERSION} sof-tplg || die

#echo "* Creating new git commit "
#git add ${SOF_VERSION} ${SOF_TPLG_VERSION} sof sof-tplg
#git commit -m "Update to ${SOF_VERSION}"
